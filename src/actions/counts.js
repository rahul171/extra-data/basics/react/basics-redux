import reducer1 from '../store/reducer/reducer1';
import reducer2 from '../store/reducer/reducer2';

export const setCountR1 = (count) => {
    return {
        type: reducer1.getActionType('count'),
        data: { count }
    };
}

export const setCountR2 = (count) => {
    return {
        type: reducer2.getActionType('count'),
        data: { count }
    };
}

export const setCountR1Async = (count) => {
    // next => when asyncMiddleware.js is used.
    // dispatch => when thunk or asyncMiddlewareThunkLike.js is used.
    return (nextOrDispatch, getState) => {
        setTimeout(() => {
            console.log('asyncMiddlewareThunkLikeGaveMeGetStatePRE', getState());
            nextOrDispatch(setCountR1(count * 10));
            console.log('asyncMiddlewareThunkLikeGaveMeGetStatePOST', getState());
        }, 2000);
    }
}
