const arrow = () => '==============>';
const tinyArrow = () => '---->';

const Redux = require('redux');

const initialState = {
    count: 0,
    name: 'rahul'
}

const reducer = (state = initialState, action) => {
    console.log(arrow(), 'reducer', action);

    const newState = {...state};
    // const newState = state;
    newState.count++;

    console.log('preState', state);
    console.log('newState', newState);
    // console.log(action);
    return newState;
}

const store = Redux.createStore(reducer);
console.log(tinyArrow(), 'getState', store.getState());

const removeListener = store.subscribe(() => {
    console.log(arrow(), 'subscribe');
    console.log('newState', store.getState());
});

console.log(removeListener);

store.dispatch({type:'hellothere', data:'fdfdfdfd'});

removeListener();

store.dispatch({type:'hellothere', data:'fdfdfdfd'});

console.log(tinyArrow(), 'getState', store.getState());
