import React from 'react';
import css from './Home.module.scss';
import ReduxStateWrapper from '../../components/ReduxState/ReduxStateWrapper';

class Home extends React.Component {
    render() {
        return (
            <div className={css.Home}>
                <ReduxStateWrapper />
            </div>
        );
    }
}

export default Home;
