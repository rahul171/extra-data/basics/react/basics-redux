export const getActionType = (type, name) => `${name}:${type}`;
export const extractActionType = (type) => type.split(':')[1];
export const isSupportedAction = (type, name) => type.split(':')[0] === name || type.startsWith('@@');

export default { getActionType, isSupportedAction, extractActionType };
