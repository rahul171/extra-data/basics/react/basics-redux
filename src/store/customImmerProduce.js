import { produce } from 'immer';

const initialState = {
    count: 0
};

export const customProduce = (fn) => {
    return (state = initialState, action, ...rest) => {
        return produce(state, (state) => {
            // here, state is a Proxy object, (immer uses it internally i guess),
            // so even if the initial state object is undefined, the draft state will be a Proxy object,
            // which is not undefined, so state in fn function will always point to Proxy.
            // hence, never setting an initial state passed in fn when fn declared in reducer2.js.
            // so, right now, to solve the issue, initialState is passed here, in this file, above.
            fn(state, action);
        });
    }
}
