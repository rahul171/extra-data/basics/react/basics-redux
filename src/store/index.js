import {
    createStore,
    applyMiddleware,
    // compose
} from 'redux';

import reducer from './reducer';

import loggerMiddleware from './middlewares/middleware1';
import loggerMiddleware2 from './middlewares/middleware2';
import asyncMiddleware from './middlewares/asyncMiddleware';
import asyncMiddlewareThunkLike from './middlewares/asyncMiddlewareThunkLike';
import thunk from 'redux-thunk';

import { composeWithDevTools } from 'redux-devtools-extension';

// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const composeEnhancers = composeWithDevTools({ trace: true, traceLimit: 25 });

const store = createStore(reducer,
    composeEnhancers(
        applyMiddleware(
            loggerMiddleware,
            loggerMiddleware2,
            // asyncMiddleware,
            asyncMiddlewareThunkLike
            // thunk
        )
    )
);
// const store = createStore(reducer, compose(applyMiddleware(loggerMiddleware, loggerMiddleware2)));
// const store = createStore(reducer, compose(applyMiddleware(loggerMiddleware), applyMiddleware(loggerMiddleware2)));

export default store;
