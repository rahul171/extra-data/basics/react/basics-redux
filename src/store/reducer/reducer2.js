import helper from '../helper';
import clone from 'lodash.clonedeep';
import { produce } from 'immer';
import { customProduce } from '../customImmerProduce';

const initialState = {
    count: 0
};

const name = 'r2';

const getActionType = (type) => helper.getActionType(type, name);
const isSupportedAction = (type) => helper.isSupportedAction(type, name);

// basic produce with immer
// const reducer = (state = initialState, action) => produce(state, (state) => {

// another type of produce with immer, which takes a state,
// const reducer = produce((state = initialState, action) => {

// tried to implement the above produce method.
// another custom type of produce, which is similar to the above one.
const reducer = customProduce((state = initialState, action) => {
    console.log('reducer2', action, action.data);

    if (!isSupportedAction(action.type)) {
        return state;
    }

    const type = helper.extractActionType(action.type);
    const data = action.data;

    // const newState = clone(state);
    // because we used immer, so we get a cloned state, not the same one.
    const newState = state;

    switch (type) {
        case 'count':
            newState.count = data.count;
            break;

        default: break;
    }

    console.log(newState);

    return newState;
});

export default { reducer, name, getActionType };
export { reducer, name, getActionType };
