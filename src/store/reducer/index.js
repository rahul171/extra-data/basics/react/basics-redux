import { combineReducers } from 'redux';

import reducer1 from './reducer1';
import reducer2 from './reducer2';

const rootReducer = combineReducers({
    [reducer1.name]: reducer1.reducer,
    [reducer2.name]: reducer2.reducer
});

export default rootReducer;
