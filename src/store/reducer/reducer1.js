import helper from '../helper';
import clone from 'lodash.clonedeep';

const initialState = {
    count: 1
};

const name = 'r1';

const getActionType = (type) => helper.getActionType(type, name);
const isSupportedAction = (type) => helper.isSupportedAction(type, name);

const reducer = (state = initialState, action) => {
    console.log('reducer1', action, action.data);

    if (!isSupportedAction(action.type)) {
        return state;
    }

    const type = helper.extractActionType(action.type);
    const data = action.data;

    // if you don't clone it, then subscribers won't be able to listen,
    // because we can't tell if the state object changed or not,
    // as it will point to the same memory space, so not change.
    // try without clone, 'shouldComponentUpdate()' won't get called in the 'ReduxSate1' class.
    // because props haven't changed (same object, so no way to determine).
    const newState = clone(state);
    // const newState = state;

    switch (type) {
        case 'count':
            newState.count = data.count;
            break;

        default: break;
    }

    console.log(newState);

    return newState;
}

export default { reducer, name, getActionType };
export { reducer, name, getActionType };
