const asyncMiddlewareThunkLike = (store) => {
    return (next) => {
        return (action) => {
            console.log('asyncMiddlewareThunkLike');
            if (typeof action === 'function') {
                action(store.dispatch, store.getState);
            } else {
                next(action);
            }
        }
    }
}

export default asyncMiddlewareThunkLike;
