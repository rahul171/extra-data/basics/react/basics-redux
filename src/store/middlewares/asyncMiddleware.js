const asyncMiddleware = (store) => {
    return (next) => {
        return (action) => {
            console.log('asyncMiddleware', action);
            if (typeof action === 'function') {
                action(next);
            } else {
                next(action);
            }
        }
    }
}

export default asyncMiddleware;
