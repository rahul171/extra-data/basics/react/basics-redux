const logger = (store) => {
    return (next) => {
        return (action) => {
            console.log('middleware =>', action);
            console.log('state', store.getState());
            // console.log(action.data.count);
            // action.data.count = 10;
            // setTimeout(() => next(action), 2000);
            next(action);
            console.log('Sync:newState', store.getState());
        }
    }
}

export default logger;
