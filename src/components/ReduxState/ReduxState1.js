import React from 'react';
// import css from './ReduxState1.module.scss';

import { connect } from 'react-redux';
import reducer1 from '../../store/reducer/reducer1';
import reducer2 from '../../store/reducer/reducer2';
import * as actions from '../../actions/counts';

class ReduxState1 extends React.Component {
    handleOnClick = (fn) => (e) => {
        fn(parseInt(e.target.value));
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('shouldComponentUpdate', nextProps);
        return true;
    }

    render() {
        console.log('render', this.props);

        return (
            <div className="ReduxState1">
                ReduxState1
                <div>countR1: {this.props.countR1}</div>
                <div>countR2: {this.props.countR2}</div>
                <div>
                    <label>
                        setCountR1
                        <input type="number" onChange={this.handleOnClick(this.props.setCountR1)} />
                    </label>
                    <label>
                        setCountR2
                        <input type="number" onChange={this.handleOnClick(this.props.setCountR2)} />
                    </label>
                    <label>
                        setCountR1Async
                        <input type="number" onChange={this.handleOnClick(this.props.setCountR1Async)} />
                    </label>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    countR1: state[reducer1.name].count,
    countR2: state[reducer2.name].count
});

const mapDispatchToProps = (dispatch) => ({
    setCountR1: (count) => { dispatch(actions.setCountR1(count)) },
    setCountR2: (count) => { dispatch(actions.setCountR2(count)) },
    setCountR1Async: (count) => { dispatch(actions.setCountR1Async(count)) },
});

export default connect(mapStateToProps, mapDispatchToProps)(ReduxState1);
