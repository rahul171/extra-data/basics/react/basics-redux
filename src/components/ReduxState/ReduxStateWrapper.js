import React from 'react';
import ReduxState1 from './ReduxState1';
// import css from './ReduxStateWrapper.module.scss';

class ReduxStateWrapper extends React.Component {
    render() {
        return (
            <div className="ReduxStateWrapper">
                <ReduxState1 />
            </div>
        );
    }
}

export default ReduxStateWrapper;
