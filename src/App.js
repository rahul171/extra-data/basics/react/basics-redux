import React from 'react';
import css from './App.module.scss';
import Home from './containers/Home/Home';

class App extends React.Component {
    render() {
        return (
            <div className={css.App}>
                <Home />
            </div>
        );
    }
}

export default App;
